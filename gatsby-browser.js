// custom typefaces
import "typeface-montserrat"
import "typeface-merriweather"
require("prismjs/themes/prism.css")
require("katex/dist/katex.min.css")

export const onClientEntry = () => {
  if (typeof window.IntersectionObserver === `undefined`) {
    import(`intersection-observer`)
  }
}