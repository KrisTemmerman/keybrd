import * as React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'
import { Typography } from '@material-ui/core'

export interface EpisodeProps {}

const EpisodeTemplate = props => {
  const episode = props.data.sanityEpisode
  return (
    <Layout>
      <SEO title={episode.title} description={episode.summary} />
      <Typography variant="body2">{props.data.sanityEpisode.title}</Typography>
    </Layout>
  )
}

export default EpisodeTemplate

export const pageQuery = graphql`
  query PodcastEpisodeBySlug($slug: String!) {
    sanityEpisode(slug: { current: { eq: $slug } }) {
      id
      internal {
        content
        description
        ignoreType
        mediaType
      }
      itunes {
        season
        type
      }
      linkList {
        URL
        excerpt
        title
      }
      subtitle
      summary
      tags
      title
      file {
        asset {
          url
        }
      }
    }
  }
`
