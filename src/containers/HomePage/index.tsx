import * as React from 'react'
import { Grid, makeStyles, Container, Typography } from '@material-ui/core'

import { HomePageQueryQuery } from '../../../@types/graphql-types'
import Logo from '../../../content/assets/logo.svg'

import ProfileCard from '../../components/ProfileCard/ProfileCard'
import PodCastFeed from '../../components/PodCastFeed/PodcastFeed'
import NavBar from '../../components/NavBar/NavBar'
import { backgroundSize } from 'styled-system'

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(4),
  },
  relative: {
    position: 'relative',
  },
  centered: {
    display: 'flex',
    justifyContent: 'center',
  },
  container: {
    paddingTop: theme.spacing(5),
  },
  mountains: {
    zIndex: 1,
  },
  content: {
    minHeight: 500,
  },
}))
interface HomePageProps {
  data: HomePageQueryQuery
}

const HomePage: React.FC<HomePageProps> = props => {
  const classes = useStyles(props)
  const {
    data: {
      allSanityHost: { edges },
    },
  } = props
  const Podcast = props.data.allSanityEpisode.edges

  return (
    <>
      <Grid containe className={classes.featuredContainer}>
        <Container maxWidth="lg">
          <NavBar />
          <Grid container className={classes.content}>
            <Grid item xs={12} sm={6}>
              <Typography variant="h5" noWrap>
                Aflevering 5 - KeyBRD website, WeWork, Project Nightingale,
                Bol.com, de launch van Disney+ en nog veel meer{' '}
              </Typography>

              <Typography variant="subtitle1" color="textSecondary">
                {Date.now()}
              </Typography>
              <Typography variant="body2">
                In deze aflevering van de KeyBRD podcast gaat de KeyBRD website
                officieel live! Daarnaast praten we ook over WeWork, Project
                Nightingale, de verkoop van kleren door Bol.com, de officiele
                launch van Disney+ en enkele sneak previews voor de podcast van
                volgende week! Check ook onze nieuwe site!
              </Typography>
            </Grid>
          </Grid>
          <div className={classes.mountains}>
            <svg
              width="1438"
              height="146"
              viewBox="0 0 1438 146"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M461 0.940513C252.6 -9.24509 66.8333 65.8131 0 104.615V119.017C66.8333 80.2143 252.6 5.15604 461 15.3416C669.4 25.5272 813.167 89.7127 859 119.017C955.667 64.4508 1234.38 0.460505 1438 156V138.567C1234.38 -16.9721 955.667 47.0183 859 101.584C813.167 72.2802 669.4 11.1261 461 0.940513Z"
                fill="#7B0000"
              />
              <path
                d="M461 12.9405C252.6 2.75491 66.8333 77.8131 0 116.615V131.017C66.8333 92.2143 252.6 17.156 461 27.3416C669.4 37.5272 813.167 101.713 859 131.017C955.667 76.4508 1234.38 12.4605 1438 168V150.567C1234.38 -4.97205 955.667 59.0183 859 113.584C813.167 84.2802 669.4 23.1261 461 12.9405Z"
                fill="#5A1111"
              />
              <path
                d="M461 26.9405C252.6 16.7549 66.8333 91.8131 0 130.615V145.017C66.8333 106.214 252.6 31.156 461 41.3416C669.4 51.5272 813.167 115.713 859 145.017C955.667 90.4508 1234.38 26.4605 1438 182V164.567C1234.38 9.02795 955.667 73.0183 859 127.584C813.167 98.2802 669.4 37.1261 461 26.9405Z"
                fill="#240505"
              />
            </svg>
          </div>
        </Container>

        <Grid container style={{ backgroundColor: '#240505' }}>
          <Container maxWidth="lg">
            <Grid container className={classes.root}>
              <Grid
                container
                justify="space-between"
                spacing={5}
                className={classes.container}
              >
                {edges.map((author, key) => (
                  <Grid item xs={12} sm={6} md={4} key={key}>
                    <ProfileCard profile={author.node} />
                  </Grid>
                ))}
              </Grid>

              <Grid container spacing={2} className={classes.feed}>
                <PodCastFeed feed={Podcast} />
              </Grid>
            </Grid>
          </Container>
        </Grid>
      </Grid>
    </>
  )
}

export default HomePage
