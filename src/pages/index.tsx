import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import SEO from '../components/seo'
import HomePage from '../containers/HomePage'
import { HomePageQueryQuery } from '../../@types/graphql-types'

interface HomepageProps {
  data: HomePageQueryQuery
}
const Homepage: React.FC<HomepageProps> = props => {
  const { data } = props
  return (
    <Layout>
      <SEO title="Home" description={data.site.siteMetadata.description} />
      <HomePage {...props} />
    </Layout>
  )
}

export default Homepage

export const pageQuery = graphql`
  query HomePageQuery {
    site {
      siteMetadata {
        title
        description
      }
    }
    allSanityHost {
      edges {
        node {
          name
          linkedin
          twitter
          instagram
          image {
            _key
            _type
            asset {
              fluid {
                src
              }
            }
          }
          _rawDescription
        }
      }
    }

    allSanityEpisode(sort: { fields: featured, order: DESC }) {
      edges {
        node {
          featured
          coverArt {
            asset {
              fluid {
                src
              }
            }
          }
          description
          duration
          explicit
          file {
            asset {
              url
            }
          }
          itunes {
            season
            type
          }
          schedule {
            publish
          }
          summary
          _rawContent
          _rawLinkList

          tags
          title
          subtitle
        }
      }
    }
  }
`
