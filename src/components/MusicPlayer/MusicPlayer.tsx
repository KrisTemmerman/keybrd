import * as React from 'react'
import { IconButton, makeStyles } from '@material-ui/core'
import { PlayArrow } from '@material-ui/icons'
import { FeedKeyBrdEnclosure } from '../../../@types/graphql-types'
import { IoIosPause } from 'react-icons/io'

const useStyles = makeStyles(theme => ({
  controls: {
    display: 'flex',
    alignItems: 'center',
    justifyItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
}))
interface IMusicPlayer {
  episode: FeedKeyBrdEnclosure
}

const useAudio = url => {
  const [audio] = React.useState(new Audio(url))
  const [playing, setPlaying] = React.useState(false)

  const toggle = () => setPlaying(!playing)

  React.useEffect(() => {
    playing ? audio.play() : audio.pause()
  }, [playing])

  return [playing, toggle]
}

const MusicPlayer: React.FC<IMusicPlayer> = ({ episode }) => {
  const classes = useStyles(undefined)
  const [playing, toggle] = useAudio(episode.url)

  return (
    <div className={classes.controls} onClick={() => toggle()}>
      <IconButton aria-label="play/pause">
        {playing ? (
          <IoIosPause className={classes.playIcon} />
        ) : (
          <PlayArrow className={classes.playIcon} />
        )}
      </IconButton>
    </div>
  )
}

export default MusicPlayer
