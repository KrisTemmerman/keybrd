import * as React from 'react'
import {
  Card,
  Avatar,
  CardContent,
  Typography,
  CardActions,
  IconButton,
  Link,
} from '@material-ui/core'
import { IoLogoLinkedin, IoLogoTwitter, IoLogoInstagram } from 'react-icons/io'
import { makeStyles } from '@material-ui/core'
import { SanityHost } from '../../../@types/graphql-types'

const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: 550,
  },
  avatar: {
    width: 150,
    height: 150,
  },
  avatarContainer: {
    display: 'flex',
    justifyContent: 'center',
    paddingTop: theme.spacing(2),
  },
  cardContent: {
    display: 'flex',
    flexDirection: 'column',
  },
  socialButtons: {
    justifyContent: 'center',
  },
}))

interface ProfileCard {
  profile: SanityHost
}

const getInitials = (name: string) => {
  const firstName = name.split(' ')[0]
  const lastName = name.split(' ')[1]
  return `${firstName.charAt(0).toUpperCase()} ${lastName
    .charAt(0)
    .toUpperCase()}`
}
const ProfileCard: React.FC<ProfileCard> = ({ profile }) => {
  const classes = useStyles(undefined)
  return (
    <Card className={classes.root}>
      <div className={classes.avatarContainer}>
        {profile.image && (
          <Avatar
            className={classes.avatar}
            src={profile.image.asset.fluid.src}
          />
        )}
        {!profile.image && (
          <Avatar className={classes.avatar}>
            {getInitials(profile.name)}
          </Avatar>
        )}
      </div>
      <CardContent className={classes.cardContent}>
        <Typography
          component="h5"
          variant="h5"
          align="center"
          color="textPrimary"
        >
          {profile.name}
        </Typography>

        <Typography variant="body1" align="center">
          {profile._rawDescription[0].children[0].text}
        </Typography>
      </CardContent>
      <CardActions className={classes.socialButtons}>
        {profile.twitter && (
          <IconButton href={profile.twitter} target={'_blank'}>
            <IoLogoTwitter />
          </IconButton>
        )}
        <IconButton href={profile.linkedin} target={'_blank'}>
          <IoLogoLinkedin />
        </IconButton>
        <IconButton href={profile.instagram} target={'_blank'}>
          <IoLogoInstagram />
        </IconButton>
      </CardActions>
    </Card>
  )
}
export default ProfileCard
