import * as React from 'react'
import { Grid } from '@material-ui/core'

import { SanityEpisodeEdge } from '../../../@types/graphql-types'
import PodCastEpisode from '../PodCastEpisode/PodCastEpisode'

interface IPodCastFeed {
  feed: SanityEpisodeEdge[]
}

const PodCastFeed: React.FC<IPodCastFeed> = ({ feed }) => {
  return (
    <Grid container spacing={3}>
      {feed.map((episode, key) =>
        episode.node.featured ? (
          <Grid item xs={12} key={key}>
            <PodCastEpisode
              featured={episode.node.featured}
              episode={episode}
            />
          </Grid>
        ) : (
          <Grid item xs={12} sm={3} key={key}>
            <PodCastEpisode
              featured={episode.node.featured}
              episode={episode}
            />
          </Grid>
        )
      )}
    </Grid>
  )
}
export default PodCastFeed
