import * as React from 'react'
import { makeStyles, AppBar, Toolbar, Link } from '@material-ui/core'
import Logo from '../../../content/assets/logo.svg'

const useStyles = makeStyles(theme => ({
  root: {
    background: 'none',
  },
  toolbar: {
    flexWrap: 'wrap',
  },

  link: {
    margin: theme.spacing(1, 1.5),
  },
  navigation: {
    ' & a:last-child ': {
      marginRight: 0,
    },
  },
}))
const NavBar = () => {
  const classes = useStyles(undefined)

  return (
    <AppBar position="static" className={classes.root} elevation={0}>
      <Toolbar className={classes.toolbar} disableGutters>
        <div style={{ flexGrow: 1 }}>
          <a href="/">
            <Logo />
          </a>
        </div>
        <nav className={classes.navigation}>
          <Link
            variant="button"
            color="textPrimary"
            href="#"
            className={classes.link}
          >
            Features
          </Link>
          <Link
            variant="button"
            color="textPrimary"
            href="#"
            className={classes.link}
          >
            Enterprise
          </Link>
          <Link
            variant="button"
            color="textPrimary"
            href="#"
            className={classes.link}
          >
            Support
          </Link>
        </nav>
      </Toolbar>
    </AppBar>
  )
}

export default NavBar
