import * as React from 'react'
import {
  Grid,
  Card,
  CardContent,
  Typography,
  ListItem,
  makeStyles,
  ListItemText,
  CardMedia,
  ListItemIcon,
  List,
  IconButton,
} from '@material-ui/core'
import { AccessAlarm, Link, PlayArrow, Pause } from '@material-ui/icons'
import ReactPlayer from 'react-player'
import PortableText from '@sanity/block-content-to-react'
const useStyles = makeStyles(theme => ({
  controls: {
    display: 'flex',
    alignItems: 'center',
    justifyItems: 'center',
    paddingLeft: theme.spacing(1),
    paddingBottom: theme.spacing(1),
  },
  playIcon: {
    height: 38,
    width: 38,
  },
  cardactions: {
    justifyContent: 'center',
  },
}))

const episodeSerializer = {
  types: {
    span: props => {
      return <Typography variant="body1">{props.node.text}</Typography>
    },
  },
  marks: {
    link: props => <Typography variant="body1">{props.href}</Typography>,
  },
}
const linkListSerializer = {
  types: {
    linkListItem: props => {
      return (
        <ListItem button component="a" href={props.node.URL} target="_blank">
          <ListItemIcon>
            <Link />
          </ListItemIcon>
          <ListItemText primary={props.node.title} />
        </ListItem>
      )
    },
  },
}

const PodCastEpisode = ({ episode, featured }) => {
  const [play, setPlay] = React.useState(false)
  const audioPlayer = React.useRef(null)

  return (
    <Card>
      <Grid container>
        <Grid item xs={12} sm={featured ? 4 : 12}>
          <CardMedia
            component="img"
            image={episode.node.coverArt.asset.fluid.src}
            title={episode.node.title}
            style={{ maxHeight: 400 }}
          />
        </Grid>
        <Grid item xs={12} sm={featured ? 8 : 12}>
          <CardContent>
            <Typography variant="h5" noWrap>
              {episode.node.title}
            </Typography>

            <Typography variant="subtitle1" color="textSecondary">
              {new Date(episode.node.schedule.publish).toUTCString()}
            </Typography>
            {featured && (
              <>
                <PortableText
                  serializers={episodeSerializer}
                  blocks={episode.node._rawContent}
                />
                <List>
                  <PortableText
                    serializers={linkListSerializer}
                    blocks={episode.node._rawLinkList}
                  />
                </List>
              </>
            )}

            <ReactPlayer
              height="10%"
              width="100%"
              url={episode.node.file.asset.url}
              file
              forceAudio
              controls
            />
          </CardContent>
        </Grid>
      </Grid>
    </Card>
  )
}
export default PodCastEpisode
