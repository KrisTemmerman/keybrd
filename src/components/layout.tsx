import React from 'react'
import ScrollToTop from 'react-scroll-up'
import { MuiThemeProvider, CssBaseline } from '@material-ui/core'
import { Link } from 'gatsby-theme-material-ui'
import theme from '../theme'

interface LayoutProps {
  children: React.ReactNode
  location?: Location
}

const Layout: React.FunctionComponent<LayoutProps> = ({ children }) => {
  if (typeof window !== 'undefined') {
    // tslint:disable-next-line: no-var-requires
    require('smooth-scroll')('a[href*="#"]')
  }
  return (
    <MuiThemeProvider theme={theme}>
      <CssBaseline />
      {children}
    </MuiThemeProvider>
  )
}

export default Layout
