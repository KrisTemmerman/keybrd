## moefit.be

moefit.be is built with GatsbyJS and TypeScript, Styled Components
Commands:


```bash
yarn  // install all the packages !!!
yarn develop    //  start gatsby develop
yarn build      //  gatsby build
yarn serve      //  gatsby serve
yarn fix         // Autofixes typescript errors
yarn lint       // runs ts lint on all files
yarn types       // Generates GraphQL types [xx]

```
[xx] Gatsby develop needs to be running to generate types


