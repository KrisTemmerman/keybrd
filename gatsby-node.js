const path = require(`path`)
const _ = require('lodash')
const { createFilePath } = require(`gatsby-source-filesystem`)

// Create episode pages based on sanity episode list and slug
exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  // define single episode

  const podcastEpisode = path.resolve(`src/templates/episode.tsx`)

  return graphql(
    `
      {
        allSanityEpisode {
          edges {
            node {
              coverArt {
                asset {
                  fluid {
                    src
                  }
                }
              }
              slug {
                current
              }
              description
              duration
              explicit
              fileUrl
              itunes {
                season
                type
              }
              schedule {
                publish
              }
              summary
              tags
              title
              subtitle
            }
          }
        }
      }
    `
  ).then(result => {
    if (result.errors) {
      throw result.errors
    }
    const episodes = result.data.allSanityEpisode.edges
    episodes.forEach((episode, index) => {
      createPage({
        path: episode.node.slug.current,
        component: podcastEpisode,
        context: {
          slug: episode.node.slug.current,
        },
      })
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    if (typeof node.frontmatter.slug !== 'undefined') {
      createNodeField({
        node,
        name: 'slug',
        value: node.frontmatter.slug,
      })
    } else {
      const value = createFilePath({ node, getNode })
      createNodeField({
        node,
        name: 'slug',
        value,
      })
    }
  }
}

// for React-Hot-Loader: react-🔥-dom patch is not detected
exports.onCreateWebpackConfig = ({ getConfig, stage }) => {
  const config = getConfig()
  if (stage.startsWith('develop') && config.resolve) {
    config.resolve.alias = {
      ...config.resolve.alias,
      'react-dom': '@hot-loader/react-dom',
    }
  }
}
